require 'rails_helper'

describe Article do
  describe "data" do
    let(:article) { create(:article) }

    it "association" do
      should belong_to(:user)
      should belong_to(:category)
      should have_many(:comments)
    end
  end
end