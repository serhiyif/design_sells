require 'rails_helper'

describe Category do
  describe "data" do
    let(:category) { create(:category) }

    it "validation" do
      should validate_presence_of(:title)
    end

    it "association" do
      should have_many(:articles)
    end
  end
end
