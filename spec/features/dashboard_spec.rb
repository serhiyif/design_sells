require 'rails_helper'

feature 'dashboard' do
  background do
    @user = create(:user)
    visit root_path
  end

  scenario "see careers page" do
    click_link('Careers')
    expect(page).to have_content('Job Title')
  end

  scenario "login" do
    expect(page).to have_content('Login')
    fill_in 'user_email', with: @user.email
    fill_in 'user_password', with: @user.password
    click_button("login")
    expect(page).to have_content('Admin menu')
  end
end
