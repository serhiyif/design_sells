require 'rails_helper'

feature 'admin menu' do
  background do
    @user = create(:user)
    visit root_path
    fill_in 'user_email', with: @user.email
    fill_in 'user_password', with: @user.password
    click_button("login")
  end

  scenario "admin menu" do
    click_link('Admin menu')
    expect(page).to have_content('Article')
    expect(page).to have_content('Career')
  end

  scenario "create article" do
    click_link('Admin menu')
    fill_in 'article_title', with: 'testing acticle'
    fill_in 'article_body', with: 'testing body'
    click_button("Add article")
    # expect(page).to have_content('testing acticle')
  end

end
