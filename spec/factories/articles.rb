FactoryGirl.define do
  factory :article do
    title 'testing'
    association :category, factory: :category
    association :user, factory: :user
  end
end