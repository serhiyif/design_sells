FactoryGirl.define do
  factory :category do
    title { Faker::Lorem.paragraph }
  end
end
