FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    password { Faker::Address.city }
  end
end