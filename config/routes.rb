Rails.application.routes.draw do

  devise_for :users, controllers: {
      sessions: 'users/sessions',
      passwords: 'users/passwords',
      registrations: 'users/registrations',
      omniauth_callbacks: 'users/omniauth_callbacks'
  }

  resources :articles do
    get :article_by_category, on: :member
  end

  resources :comments

  resources :careers

  resources :dashboard do
    get :admin_menu, on: :collection
  end

  root :to => "dashboard#index"
end
