source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.0.1'
gem 'puma', '~> 3.0'
gem 'sass-rails', '~> 5.0'
gem 'haml'
gem 'bootstrap', '~> 4.0.0.alpha5'
gem 'bootstrap-sass'
gem 'sprockets-rails', :require => 'sprockets/railtie'
gem 'rails-assets-tether', '>= 1.1.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'

gem 'jquery-rails'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'pg'
gem 'devise'
gem 'devise_token_auth'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'simple_form'
gem 'nested_form'
gem 'faker'
gem 'factory'
gem 'figaro'
gem 'protected_attributes_continued'
gem 'paperclip', '~> 5.0.0'
gem 'clamav-client', require: 'clamav/client'

group :development, :test do
  gem 'byebug', platform: :mri
  gem 'pry-byebug'
end

group :test do
  gem 'rspec-rails', '~> 3.0'
  gem 'shoulda', '~> 3.5'
  gem 'shoulda-matchers', '~> 2.0'
  gem 'factory_girl'
  gem 'factory_girl_rails'
  gem 'database_cleaner'
  gem 'simplecov', :require => false
  gem 'simplecov-rcov', :require => false
  gem 'capybara'
  # gem 'capybara-webkit'
  # gem 'selenium-webdriver'
  gem "poltergeist"
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
