class Article < ApplicationRecord
  has_many :comments, :as => :commentable

  belongs_to :user
  belongs_to :category

  has_attached_file :image, styles: { medium: "740x740>", thumb: "100x100>" }, default_url: "/1122133.jpg"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  attr_accessible :title, :body, :user_id, :category_id, :image

  accepts_nested_attributes_for :comments

end
