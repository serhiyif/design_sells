class Career < ApplicationRecord
  has_many :key_qualifications

  attr_accessible :title,
                  :description,
                  :education,
                  :key_qualifications_attributes

  accepts_nested_attributes_for :key_qualifications
end
