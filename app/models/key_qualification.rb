class KeyQualification < ApplicationRecord

  belongs_to :career

  attr_accessible :body,
                  :career_id

end
