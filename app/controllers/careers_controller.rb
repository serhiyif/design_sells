class CareersController < ApplicationController
  ActionController::Parameters.permit_all_parameters = true

  def index
    @careers = Career.all
  end

  def show
    @career = Career.find(params[:id])
  end

  def create
    Career.create(params[:career])
    id = Career.last.id
    redirect_to career_path(id)
  end

end
