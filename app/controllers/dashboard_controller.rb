class DashboardController < ApplicationController
  def index
    @categories = Category.all
    @articles = Article.all
  end

  def admin_menu
  end
end
