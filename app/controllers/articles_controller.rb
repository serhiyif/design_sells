class ArticlesController < ApplicationController

  ActionController::Parameters.permit_all_parameters = true

  before_action :find_article, only: [:update]

  def update
    @article.comments.create(params[:article][:comment])
    redirect_to :back
  end

  def create
    Article.create(params[:article]) unless detected_virus?
    redirect_to :root
  end

  def article_by_category
    @categories = Category.all
    @articles = Category.find(params[:id]).articles
    respond_to do |format|
      format.js
    end
  end

  private

  def detected_virus?
    # https://github.com/franckverrot/clamav-client
    client = ClamAV::Client.new
    io = StringIO.new(params[:article][:image].tempfile.read)
    raise "detected virus" if client.execute(ClamAV::Commands::InstreamCommand.new(io)).virus_name.present?
  end

  def find_article
    @article = Article.find(params[:id])
  end

end
