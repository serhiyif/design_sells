class CreateCareers < ActiveRecord::Migration[5.0]
  def change
    create_table :careers do |t|
      t.string :title
      t.text :summary
      t.text :description
      t.text :education
      t.timestamps
    end
  end
end
