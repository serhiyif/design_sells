class CreateKeyQualifications < ActiveRecord::Migration[5.0]
  def change
    create_table :key_qualifications do |t|
      t.text :body
      t.belongs_to :career, index: true
      t.timestamps
    end
  end
end
