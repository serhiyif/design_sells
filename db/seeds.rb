# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create(email: 'serhiy@gmail.com', password: 111111)

@article = Article.new(title: 'Good Design Sells', body: Faker::Lorem.paragraph)

user.articles << @article

10.times do
  @career = Career.create(title: Faker::Lorem.paragraph, summary: Faker::Lorem.paragraph, \
    description: Faker::Lorem.paragraph, education: Faker::Educator.course)
  10.times { @career.key_qualifications << KeyQualification.new(body: Faker::Lorem.sentence) }
end

@category = Category.create(title: 'Web Design')

Category.create(title: 'Interior Design')

@category.articles << @article
